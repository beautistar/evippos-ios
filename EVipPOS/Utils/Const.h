//
//  Const.h
//  EVipPOS
//
//  Created by AOC on 03/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Const : NSObject




/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

// Array const
//pinkColor


// string const

#define SAVE_ROOT_PATH                      @"EVip-POS"

#define CONN_ERROR                          @"Connect server fail.\n Please try again when you are online."

#define ALERT_FORGOTPWD_TITLE               @"Enter your email"
#define ALERT_TITLE                         @"E-Vip POS"
#define ALERT_OK                            @"OK"
#define ALERT_CANCEL                        @"Cancel"
#define INPUT_USERNAME                      @"Please input User Name."
#define INPUT_PWD                           @"Please input Password."

#define INPUT_COMPANY                       @"Please input company."
#define INPUT_PHONE1                        @"Please input phone number 1."
#define INVALID_EDITCUSTOMER                @"Invalid to edit customer."
#define INVALID_DELCUSTOMER                 @"Invalid to delete customer."
#define INVALID_STYLECODE                   @"Style code never be blank."
#define INVALID_CUSTOMER                    @"Customer never be blank."
#define INVALID_PRICE                       @"Price never be blank."
#define INVALID_COLOR                       @"Color never be blank"
#define INVALID_QTY                         @"Quantity never be blank"
#define INPUT_NOTE                          @"Input note."
#define INPUT_MAIL                          @"Input mail"


#define RES_UNREGISTERED_USERNAME           @"Unregistered username."
#define CHECK_PWD                           @"Wrong password."
#define EXIST_CUS_NAME                      @"Customer company or phone1 already exist."
#define CONFIRM_CANCEL                      @"Are you sure you want to cancel order?"


@end
