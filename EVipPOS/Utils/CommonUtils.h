//
//  CommonUtils.h
//  EVipPOS
//
//  Created by AOC on 13/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonUtils : NSObject

+ (BOOL) isValidEmail: (NSString *) email;

// save image to file with size specification
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile;



@end
