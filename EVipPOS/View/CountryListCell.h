//
//  CountryListCell.h
//  EVipPOS
//
//  Created by AOC on 06/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvFlag;

@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end
