//
//  ColorPadCell.h
//  EVipPOS
//
//  Created by AOC on 13/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorPadCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvShape;
@property (weak, nonatomic) IBOutlet UIImageView *imvCheck;

- (void) setShapeColor : (UIColor *) color;


@end
