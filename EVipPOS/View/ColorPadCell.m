//
//  ColorPadCell.m
//  EVipPOS
//
//  Created by AOC on 13/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ColorPadCell.h"

@implementation ColorPadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    // set layout inset to zero
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:animated];
    
    // Configure the view for the selected state
}


- (void) setShapeColor : (UIColor *) color {
    
    self.imvShape.image = [self.imvShape.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.imvShape setTintColor:color];
    
}

@end
