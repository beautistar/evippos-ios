//
//  ReceiptHeaderCell.m
//  EVipPOS
//
//  Created by AOC on 12/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ReceiptHeaderCell.h"

@implementation ReceiptHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.layoutMargins = UIEdgeInsetsZero;
    self.preservesSuperviewLayoutMargins = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
