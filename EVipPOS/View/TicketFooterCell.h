//
//  TicketFooterCell.h
//  EVipPOS
//
//  Created by AOC on 15/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketFooterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnGoDetail;

@end
