//
//  ReceiptHeaderCell.h
//  EVipPOS
//
//  Created by AOC on 12/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;

@end
