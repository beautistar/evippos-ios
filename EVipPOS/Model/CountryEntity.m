//
//  CountryEntity.m
//  EVipPOS
//
//  Created by AOC on 09/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "CountryEntity.h"

@implementation CountryEntity

@synthesize _name, _code;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        
        _name = @"";
        _code = @"";
    }
    
    return self;

}

@end
