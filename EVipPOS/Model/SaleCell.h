//
//  SaleCell.h
//  EVipPOS
//
//  Created by AOC on 12/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvSale;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

- (void) setImageColor : (UIColor *) color;

@end
