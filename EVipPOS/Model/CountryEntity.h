//
//  CountryEntity.h
//  EVipPOS
//
//  Created by AOC on 09/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryEntity : NSObject

@property (nonatomic, strong) NSString *_name;

@property (nonatomic, strong) NSString *_code;

//@property (nonatomic, strong) NSString *name;

@end
