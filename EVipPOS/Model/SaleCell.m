//
//  SaleCell.m
//  EVipPOS
//
//  Created by AOC on 12/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "SaleCell.h"

@implementation SaleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    // set layout inset to zero
    
    self.layoutMargins = UIEdgeInsetsZero;
    self.preservesSuperviewLayoutMargins = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setImageColor : (UIColor *) color {
    
    self.imvSale.image = [self.imvSale.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.imvSale setTintColor:color];
}

@end
