//
//  DiscountListViewController.m
//  EVipPOS
//
//  Created by AOC on 14/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "DiscountListViewController.h"
#import "SaleCell.h"

@interface DiscountListViewController () <UITableViewDataSource, UITableViewDelegate> {
    
    
    __weak IBOutlet UITableView *tblDiscountList;
    
}

@end

@implementation DiscountListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.0;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SaleCell *cell = (SaleCell *) [tableView dequeueReusableCellWithIdentifier:@"SaleCell"];
    
    return cell;
}

@end
