//
//  CategoryDetailViewController.m
//  EVipPOS
//
//  Created by AOC on 13/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "CategoryDetailViewController.h"
#import "ColorPadCell.h"

@interface CategoryDetailViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
    
    __weak IBOutlet UIView *vName;
    __weak IBOutlet UIView *vColor;
    NSArray *colors;
    int selectedIndex;
}

@end

@implementation CategoryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    colors = [[NSArray alloc] initWithObjects:
                       [UIColor colorWithRed:226.0/255 green:226.0/255 blue:226.0/255 alpha:1.0],
                       [UIColor colorWithRed:244.0/255 green:68.0/255 blue:56.0/255 alpha:1.0],
                       [UIColor colorWithRed:233.0/255 green:32.0/255 blue:100.0/255 alpha:1.0],
                       [UIColor colorWithRed:255.0/255 green:153.0/255 blue:2.0/255 alpha:1.0],
                       [UIColor colorWithRed:205.0/255 green:220.0/255 blue:59.0/255 alpha:1.0],
                       [UIColor colorWithRed:77.0/255 green:176.0/255 blue:81.0/255 alpha:1.0],
                       [UIColor colorWithRed:69.0/255 green:139.0/255 blue:255.0/255 alpha:1.0],
                       [UIColor colorWithRed:157.0/255 green:41.0/255 blue:177.0/255 alpha:1.0], nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    //shadow effect
    vName.layer.shadowColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
    vName.layer.shadowOffset = CGSizeMake(2, 2);
    vName.layer.shadowOpacity = 1;
    vName.layer.shadowRadius = 1.0;
    
    vColor.layer.shadowColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
    vColor.layer.shadowOffset = CGSizeMake(2, 2);
    vColor.layer.shadowOpacity = 1;
    vColor.layer.shadowRadius = 1.0;
    

    
    
}


- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)saveAction:(id)sender {
    

}

#pragma mark - collectionview datasource & delegate

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 8;
}


- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float cellWidth = collectionView.frame.size.width / 4.0f - 10;
    float cellHeight = 60.0;
    if (cellWidth < 60.0) {
        cellHeight = cellWidth;
    }
    return CGSizeMake(cellWidth, cellHeight);
    
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ColorPadCell *cell = (ColorPadCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"ColorPadCell" forIndexPath:indexPath];
    
    [cell setShapeColor:(UIColor *) colors[indexPath.row]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ColorPadCell *cell = (ColorPadCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [cell.imvCheck setHidden:NO];
    
    selectedIndex = (int)indexPath.row;
    
}

- (void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ColorPadCell *cell = (ColorPadCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [cell.imvCheck setHidden:YES];
}





@end
