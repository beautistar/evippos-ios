//
//  ItemListViewController.m
//  EVipPOS
//
//  Created by AOC on 12/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ItemListViewController.h"
#import "CreateItemViewController.h"
#import "VCFloatingActionButton.h"
#import "SaleCell.h"


@interface ItemListViewController () <floatMenuDelegate, UITableViewDataSource, UITableViewDelegate> {
    
    __weak IBOutlet UITableView *tblItemList;
    
}
@property (strong, nonatomic) VCFloatingActionButton *addButton;

@end


@implementation ItemListViewController
@synthesize addButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // remove empty cell
    tblItemList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

}

- (void) initView {
    
    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 20, [UIScreen mainScreen].bounds.size.height - 44 - 20, 44, 44);
    
    addButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"plus"] andPressedImage:[UIImage imageNamed:@"plus"] withScrollview:tblItemList];
    
    
    addButton.hideWhileScrolling = YES;
    addButton.delegate = self;
    
    
    id tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didFloatingButtonTapped:)];
    [tap setNumberOfTapsRequired:1];
    [addButton addGestureRecognizer:tap];
    
    [self.view addSubview:addButton];
    

}

-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    NSLog(@"Floating action tapped index %tu",row);
    
    
}


- (void) didFloatingButtonTapped: (id) sender {
    
    NSLog(@"floating button tapped");
    
    CreateItemViewController *destVC = (CreateItemViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"CreateItemViewController"];
    
    [self.navigationController pushViewController:destVC animated:YES];
}


- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 20;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.0;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    SaleCell *cell = (SaleCell *) [tableView dequeueReusableCellWithIdentifier:@"SaleCell"];
    
    if (indexPath.row % 2 == 0) {
        
        [cell setImageColor:[UIColor redColor]];
        
    } else {
        
        [cell setImageColor:[UIColor blueColor]];
        
    }
    return cell;
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
