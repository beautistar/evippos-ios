//
//  DiscountDetailViewController.m
//  EVipPOS
//
//  Created by AOC on 14/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "DiscountDetailViewController.h"

@interface DiscountDetailViewController () {
    
    __weak IBOutlet UILabel *lblTitle;
    
}

@end

@implementation DiscountDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)segmentSwitchAction:(UISegmentedControl *)sender {
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) { // %
        
        NSLog(@"percent selected");
        
    } else {// Σ
        
        NSLog(@"Sigma selected");
    }
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
