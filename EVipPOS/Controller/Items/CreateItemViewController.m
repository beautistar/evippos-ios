//
//  CreateItemViewController.m
//  EVipPOS
//
//  Created by AOC on 12/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "CreateItemViewController.h"
#import "ColorPadCell.h"

#import "CommonUtils.h"

@interface CreateItemViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    
    __weak IBOutlet UIView *vItemDescription;
    __weak IBOutlet UIImageView *imvEach;
    __weak IBOutlet UIImageView *imvWeight;
    __weak IBOutlet UIView *vPos;
    
    __weak IBOutlet UIImageView *imvColor;
    __weak IBOutlet UIImageView *imvImage;
    __weak IBOutlet UIView *vChoosePicture;
    __weak IBOutlet UIView *vTakeCamera;
    __weak IBOutlet UIView *vImage;
    __weak IBOutlet UIImageView *imvPhoto;
    __weak IBOutlet UICollectionView *cvColorPad;
    
    NSArray *colors;
    int selectedIndex;
    NSString *photoPath;    
    
}

@end

@implementation CreateItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    colors = [[NSArray alloc] initWithObjects:
                [UIColor colorWithRed:226.0/255 green:226.0/255 blue:226.0/255 alpha:1.0],
                [UIColor colorWithRed:244.0/255 green:68.0/255 blue:56.0/255 alpha:1.0],
                [UIColor colorWithRed:233.0/255 green:32.0/255 blue:100.0/255 alpha:1.0],
                [UIColor colorWithRed:255.0/255 green:153.0/255 blue:2.0/255 alpha:1.0],
                [UIColor colorWithRed:205.0/255 green:220.0/255 blue:59.0/255 alpha:1.0],
                [UIColor colorWithRed:77.0/255 green:176.0/255 blue:81.0/255 alpha:1.0],
                [UIColor colorWithRed:69.0/255 green:139.0/255 blue:255.0/255 alpha:1.0],
                [UIColor colorWithRed:157.0/255 green:41.0/255 blue:177.0/255 alpha:1.0], nil];
    
    [self initView];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    //shadow effect
    vItemDescription.layer.shadowColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
    vItemDescription.layer.shadowOffset = CGSizeMake(2, 2);
    vItemDescription.layer.shadowOpacity = 1;
    vItemDescription.layer.shadowRadius = 1.0;
    
    vPos.layer.shadowColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
    vPos.layer.shadowOffset = CGSizeMake(2, 2);
    vPos.layer.shadowOpacity = 1;
    vPos.layer.shadowRadius = 1.0;
    
    //set borders
    vChoosePicture.layer.borderWidth = 0.5f;
    vChoosePicture.layer.borderColor = [[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1.0] CGColor];
    
    vTakeCamera.layer.borderWidth = 0.5f;
    vTakeCamera.layer.borderColor = [[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1.0] CGColor];
    
    //image view border setting
    imvPhoto.layer.borderWidth = 1;
    imvPhoto.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [imvPhoto setHidden:YES];
}


- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)eachAction:(id)sender {
    
    imvEach.image = [UIImage imageNamed:@"ic_radio_on"];
    imvWeight.image = [UIImage imageNamed:@"ic_radio_off"];
}


- (IBAction)weightAction:(id)sender {
    
    imvEach.image = [UIImage imageNamed:@"ic_radio_off"];
    imvWeight.image = [UIImage imageNamed:@"ic_radio_on"];
}

- (IBAction)colorAction:(id)sender {
    
    imvColor.image = [UIImage imageNamed:@"ic_radio_on"];
    imvImage.image = [UIImage imageNamed:@"ic_radio_off"];
    [vImage setHidden:YES];
    [imvPhoto setHidden:YES];
    
}

- (IBAction)imageAction:(id)sender {
    
    imvColor.image = [UIImage imageNamed:@"ic_radio_off"];
    imvImage.image = [UIImage imageNamed:@"ic_radio_on"];
    [vImage setHidden:NO];
    //[imvPhoto setHidden:NO];
}

- (IBAction)selectShapeAction:(id)sender {
    
    UIButton *btnSelected = (UIButton *) sender ;
    
    for (int i = 1 ; i <= 4 ; i++) {
        
        UIImageView *imvShape = (UIImageView *) [self.view viewWithTag:i];
        
        if (btnSelected.tag != i + 20 ) {
            
            [imvShape setHidden:YES];
            
        } else {
            
            [imvShape setHidden:NO];
        }
    }
}

- (IBAction)doneAction:(id)sender {
}

- (IBAction)cameraAction:(id)sender {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (IBAction)galleryAction:(id)sender {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
    
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage isProfile:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                photoPath = strPhotoPath;
                
                // update ui (set profile image with saved Photo URL
                [imvPhoto setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
                [imvPhoto setHidden:NO];
            });
        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - collectionview datasource & delegate

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 8;
}


- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float cellWidth = collectionView.frame.size.width / 4.0f - 10;
    float cellHeight = 60.0;
    if (cellWidth < 60.0) {
        cellHeight = cellWidth;
    }
    return CGSizeMake(cellWidth, cellHeight);
    
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ColorPadCell *cell = (ColorPadCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"ColorPadCell" forIndexPath:indexPath];
    
    [cell setShapeColor:(UIColor *) colors[indexPath.row]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  
    ColorPadCell *cell = (ColorPadCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [cell.imvCheck setHidden:NO];
    
    selectedIndex = (int)indexPath.row;
    
}

- (void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ColorPadCell *cell = (ColorPadCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [cell.imvCheck setHidden:YES];
}





@end
