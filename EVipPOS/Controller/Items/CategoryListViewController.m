//
//  CategoryListViewController.m
//  EVipPOS
//
//  Created by AOC on 13/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "CategoryListViewController.h"

#import "SaleCell.h"

@interface CategoryListViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UITableView *tblCategoryList;
    
}

@end

@implementation CategoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Tableview datasource & delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.0;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SaleCell *cell = (SaleCell *) [tableView dequeueReusableCellWithIdentifier:@"SaleCell"];
    
    cell.lblPrice.hidden = YES;
    //cell.imvSale.hidden = YES;
    
    return cell;
}

@end
