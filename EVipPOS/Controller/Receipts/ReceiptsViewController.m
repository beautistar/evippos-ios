//
//  ReceiptsViewController.m
//  EVipPOS
//
//  Created by AOC on 10/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ReceiptsViewController.h"
#import "ReceiptHeaderCell.h"
#import "ReceiptCell.h"


@interface ReceiptsViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UITableView *tblReceiptList;
    
}

@end

@implementation ReceiptsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.0 green:162/255.0 blue:0/255.0 alpha:1.0];
    
    tblReceiptList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

#pragma mark - tableview delegate

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 5;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 60.0f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.0f;
}



- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    ReceiptHeaderCell * headerCell = (ReceiptHeaderCell *)[tableView dequeueReusableCellWithIdentifier:@"ReceiptHeaderCell"];
    
    return headerCell;
    
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReceiptCell *cell = (ReceiptCell *) [tableView dequeueReusableCellWithIdentifier:@"ReceiptCell"];
    return cell;
}



@end
