//
//  FeedbackViewController.m
//  EVipPOS
//
//  Created by AOC on 10/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "FeedbackViewController.h"

@interface FeedbackViewController () {
    
    UIColor *selectedColor ;
    
    NSInteger selectedState;
}

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    selectedColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1/255.0 green:162/255.0 blue:1/255.0 alpha:1.0];
}


- (IBAction)changeAction:(id)sender {
    
    UIButton *btn = (UIButton *) sender;
    
    for (int i = 0 ; i < 4 ; i++) {
        
        UIView *bgView = [self.view viewWithTag:i + 10];
        
        if (btn.tag == i + 20) {
            
            bgView.backgroundColor = selectedColor;
            
            selectedState = i;
        
        } else {
            
            bgView.backgroundColor = [UIColor whiteColor];            
            
        }
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
