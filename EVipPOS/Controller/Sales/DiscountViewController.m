//
//  DiscountViewController.m
//  EVipPOS
//
//  Created by AOC on 15/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "DiscountViewController.h"
#import "DiscountCell.h"

@interface DiscountViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UITableView *tblDiscountList;
    
}

@end

@implementation DiscountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //remove empty cells
    tblDiscountList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview datasource & delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DiscountCell *cell = (DiscountCell *) [tableView dequeueReusableCellWithIdentifier:@"DiscountCell"];
    
    return cell;  
    
}

@end
