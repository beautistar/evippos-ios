//
//  FavoriteSalesViewController.m
//  EVipPOS
//
//  Created by AOC on 11/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "FavoriteSalesViewController.h"
#import "SaleCell.h"

@interface FavoriteSalesViewController () <UITableViewDataSource, UITableViewDelegate> {
    
    __weak IBOutlet UITableView *tblSaleList;
    
}

@end

@implementation FavoriteSalesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // remove empty cell
    tblSaleList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 7;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.0;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    SaleCell *cell = (SaleCell *) [tableView dequeueReusableCellWithIdentifier:@"SaleCell"];
    
    if (indexPath.row % 2 == 0) {
        [cell setImageColor:[UIColor redColor]];
    } else {
        
        [cell setImageColor:[UIColor blueColor]];
        
    }
    
    return cell;    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
