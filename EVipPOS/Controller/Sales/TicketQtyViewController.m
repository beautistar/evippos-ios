//
//  TicketQtyViewController.m
//  EVipPOS
//
//  Created by AOC on 15/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "TicketQtyViewController.h"

@interface TicketQtyViewController () {
    
    __weak IBOutlet UIButton *btnMinus;
    __weak IBOutlet UITextField *tfQty;
    __weak IBOutlet UIButton *btnPlus;
    
    int _qty;
}

@end

@implementation TicketQtyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _qty = 1;
    
    btnMinus.layer.borderColor = [[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0] CGColor];
    btnMinus.layer.borderWidth = 0.5;
    
    btnPlus.layer.borderColor = [[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0] CGColor];
    btnPlus.layer.borderWidth = 0.5;
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)okAction:(id)sender {
    
}

- (IBAction)minusAction:(id)sender {
    
    if (_qty > 0) _qty--;
    tfQty.text = [NSString stringWithFormat:@"%d", _qty];
    
}

- (IBAction)plusAction:(id)sender {
    
    _qty++;
    tfQty.text = [NSString stringWithFormat:@"%d", _qty];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
