//
//  SalesViewController.m
//  EVipPOS
//
//  Created by AOC on 10/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "SalesViewController.h"
#import "TicketQtyViewController.h"
#import "DiscountViewController.h"

#import "CarbonKit.h"

#import "TicketCell.h"
#import "TicketFooterCell.h"

#import "UIViewMYPop.h"


@interface SalesViewController () <CarbonTabSwipeNavigationDelegate, UITableViewDataSource, UITableViewDelegate> {
    
    BOOL isTicketState;
    
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    
    __weak IBOutlet UITableView *tblTicketList;
    __weak IBOutlet UIView *ticketView;
    __weak IBOutlet UIView *vContent;
    __weak IBOutlet UIButton *btnDelete;
    __weak IBOutlet UIButton *btnSearch;
    __weak IBOutlet UILabel *lblChargeAmount;
    __weak IBOutlet UIView *vSearch;
    
}

@end

@implementation SalesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isTicketState = false;
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1/255.0 green:162/255.0 blue:1/255.0 alpha:1.0];
    
    
    if (isTicketState) {
        
        [ticketView setHidden:NO];
        [btnDelete setHidden:NO];
        [btnSearch setHidden:YES];
        
    } else {
        [ticketView setHidden:YES];
        [btnDelete setHidden:YES];
        [btnSearch setHidden:NO];
    }
    
    //set label right image
//    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
//    attachment.image = [UIImage imageNamed:@"ic_apply_white"];
//    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
//    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"charge 3000"];
//    [str appendAttributedString:attachmentString];
//    lblChargeAmount.attributedText = str;
    
}

- (void) initView {
    
    [vSearch setHidden:YES];
    
    
    items = @[@"All", @"Favorite"];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    //[carbonTabSwipeNavigation insertIntoRootViewController:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:vContent];
    UIColor *color = [UIColor colorWithRed:0.0 / 255.0 green:162.0 / 255 blue:0.0 / 255 alpha:1];
    // set catbonkit style
    carbonTabSwipeNavigation.toolbarHeight.constant = 40;
    carbonTabSwipeNavigation.toolbar.barTintColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1.0];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:color];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width / 2 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width / 2 forSegmentAtIndex:1];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.6]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color font:[UIFont boldSystemFontOfSize:14]];
    
    
}

#pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            NSLog(@"index 0");
            return [self.storyboard instantiateViewControllerWithIdentifier:@"AllSalesViewController"];
            
            
        default: return [self.storyboard instantiateViewControllerWithIdentifier:@"FavoriteSalesViewController"];
            
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    //    switch (index) {
    //        case 0:
    //            self.title = @"Home";
    //            break;
    //
    //        default:
    //            self.title = items[index];
    //            break;
    //    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %d", (int)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}

- (IBAction)newTicketAction:(id)sender {

    NSLog(@"%@", @"new ticket");
    
    if (!isTicketState) {
        
        [ticketView setHidden:NO];
        [btnDelete setHidden:NO];
        [btnSearch setHidden:YES];
        
        isTicketState = !isTicketState;
    
    } else {
        
        [ticketView setHidden:YES];
        [btnDelete setHidden:YES];
        [btnSearch setHidden:NO];
        
        isTicketState = !isTicketState;
    }
}

- (IBAction)menuAction:(id)sender {

    NSLog(@"%@", @"menu");
}

- (IBAction)addNewClientAction:(id)sender {
    
    NSLog(@"%@", @"new add new");
    
    [vSearch setHidden:NO];
    //[vSearch showWithAnimationType:MYPopAnimationTypeFade];
}

- (IBAction)searchAction:(id)sender {
    
    
    
    NSLog(@"%@", @"search");
}

- (IBAction)chargeConfirmAction:(id)sender {
    
    
}

#pragma mark - tableview datasource & delegate
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 20;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 50.0;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TicketCell * cell = (TicketCell *) [tableView dequeueReusableCellWithIdentifier:@"TicketCell"];
    return cell;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    TicketFooterCell *footerCell = (TicketFooterCell *) [tableView dequeueReusableCellWithIdentifier:@"TicketFooterCell"];
    
    [footerCell.btnGoDetail addTarget:self action:@selector(footerTapped) forControlEvents:UIControlEventTouchUpInside];
    
    return footerCell;
}

- (void) footerTapped {
    
    DiscountViewController *destVC = (DiscountViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"DiscountViewController"];
    [self.navigationController pushViewController:destVC animated:YES];
    
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TicketQtyViewController *destVC = (TicketQtyViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"TicketQtyViewController"];
    
    [self.navigationController pushViewController:destVC animated:YES];
}


@end
