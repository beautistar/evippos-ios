//
//  RegisterViewController.m
//  EVipPOS
//
//  Created by AOC on 03/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "RegisterViewController.h"
#import "M13Checkbox.h"
#import "CountryListCell.h"
#import "CountryEntity.h"

@interface RegisterViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UIView *vAgree;
    
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfPasswordCfm;
    __weak IBOutlet UITextField *tfPassword;
    
    __weak IBOutlet UITextField *tfBusinessName;
    
    __weak IBOutlet UITextField *tfCountry;
    
    __weak IBOutlet UIView *vBg;
    __weak IBOutlet UITableView *tblCountryList;
    __weak IBOutlet UIImageView *imvFlag;
    __weak IBOutlet UILabel *lblCountryName;
    NSMutableArray *countries;
}

@property (nonatomic, strong) M13Checkbox *chbTerm;

@end

@implementation RegisterViewController


- (void) viewDidLoad {

    [super viewDidLoad];
    
    [self initData];
    [self initView];

}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void) initView {
    
    [self.navigationController setNavigationBarHidden:NO];
    
    _chbTerm = [[M13Checkbox alloc] initWithTitle:nil];
    [_chbTerm setCheckAlignment:M13CheckboxAlignmentLeft];
    _chbTerm.strokeColor = [UIColor blackColor];
    _chbTerm.checkColor = [UIColor blackColor];
    _chbTerm.titleLabel.textColor = [UIColor blackColor];
    
    _chbTerm.tintColor = [UIColor clearColor];
    _chbTerm.frame = CGRectMake(0, 8, _chbTerm.frame.size.width, _chbTerm.frame.size.height * 0.75);
    [_chbTerm addTarget:self action:@selector(gotoTerm:) forControlEvents:UIControlEventValueChanged];
    
    [vAgree addSubview:_chbTerm];
}

- (void) initData {
    
    countries = [[NSMutableArray alloc] init];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"];
    NSError *error;
    NSString *fileContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        
        NSLog(@"Error reading file : %@", error.localizedDescription);
    }
    
    NSArray *dataList = (NSArray *)[NSJSONSerialization JSONObjectWithData:[fileContent dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
    
    for (int i = 0 ; i < dataList.count ; i++) {
        
        CountryEntity * country = [[CountryEntity alloc] init];
        id keyValuePair = dataList[i];
        country._name = keyValuePair[@"name"];
        country._code = keyValuePair[@"code"];
        
        [countries addObject:country];
    }
}

- (void) gotoTerm :(id) sender {
    
    
}
- (IBAction)selectCountry:(id)sender {
    
    [self.navigationController setNavigationBarHidden:YES];
    [vBg setHidden:NO];
    
    [tblCountryList setHidden:NO];
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    if ([tfBusinessName isFirstResponder]) {
        
        return 180;
    } else if([tfPassword isFirstResponder] || [tfPasswordCfm isFirstResponder]) {
        
        return 200;
    }
    
    return 240;
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview datasource & delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return countries.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 40.0f;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CountryListCell *cell = (CountryListCell *) [tableView dequeueReusableCellWithIdentifier:@"CountryListCell"];
    
    
    
    CountryEntity *country = (CountryEntity *)countries[indexPath.row];
    cell.lblName.text = country._name;
    
    NSString *codeStr = [country._code lowercaseString];
    
    [cell.imvFlag setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@%@", @"ic_flag_flat_", codeStr]]];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CountryEntity *country = (CountryEntity *)countries[indexPath.row];
    lblCountryName.text = country._name;
    
    NSString *codeStr = [country._code lowercaseString];
    
    [imvFlag setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@%@", @"ic_flag_flat_", codeStr]]];
    
    [tblCountryList setHidden:YES];
    [self.navigationController setNavigationBarHidden:NO];
    [vBg setHidden:YES];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.navigationController setNavigationBarHidden:NO];
    [tblCountryList setHidden:YES];
    
    [vBg setHidden:YES];
    
    [self.view endEditing:YES];
}

@end
