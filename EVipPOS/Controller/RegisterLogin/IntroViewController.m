//
//  IntroViewController.m
//  EVipPOS
//
//  Created by AOC on 03/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "IntroViewController.h"

@interface IntroViewController () {
    
    __weak IBOutlet UIButton *btnRegister;
    
}

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIColor *color = [UIColor colorWithRed:140/255.0f green:200/255.0f blue:35/255.0f alpha:1.0]
    ;
    
    btnRegister.layer.borderColor = [color CGColor];
    btnRegister.layer.borderWidth = 1;
    btnRegister.layer.masksToBounds = YES;
    
    [self.navigationController setNavigationBarHidden:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
