//
//  LoginViewController.m
//  EVipPOS
//
//  Created by AOC on 03/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "LoginViewController.h"
#import "Const.h"

@interface LoginViewController () {
    
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.navigationController setNavigationBarHidden:NO];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event  {
    
    [self.view endEditing:YES];
}

- (IBAction)forgotPasswordAction:(id)sender {
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: ALERT_FORGOTPWD_TITLE
                                                                              message: nil
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"email";
//        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:ALERT_OK style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * email = textfields[0];
//        UITextField * passwordfiled = textfields[1];
        NSLog(@"%@",email.text);
        [self resetPassword:email.text];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:ALERT_CANCEL style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void) resetPassword : (NSString *) email {
    
    NSLog(@"%@",email);
    
    
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)enterAction:(id)sender {
    [self gotoMain];
    
    
}

- (void) gotoMain {
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"RootViewController"]];
}

@end
