//
//  LeftMenuViewController.h
//  EVipPOS
//
//  Created by AOC on 10/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface LeftMenuViewController : UIViewController <RESideMenuDelegate>


@end
