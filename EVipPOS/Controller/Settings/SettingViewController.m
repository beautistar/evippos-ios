//
//  SettingViewController.m
//  EVipPOS
//
//  Created by AOC on 10/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController () {
    
    __weak IBOutlet UIView *contentView;
    
    __weak IBOutlet UIButton *signoutButton;
}

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //shadow effect
    contentView.layer.shadowColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
    contentView.layer.shadowOffset = CGSizeMake(2, 2);
    contentView.layer.shadowOpacity = 1;
    contentView.layer.shadowRadius = 1.0;
    
    //shadow effect
    signoutButton.layer.shadowColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
    signoutButton.layer.shadowOffset = CGSizeMake(2, 2);
    signoutButton.layer.shadowOpacity = 1;
    signoutButton.layer.shadowRadius = 1.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1/255.0 green:162/255.0 blue:1/255.0 alpha:1.0];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
