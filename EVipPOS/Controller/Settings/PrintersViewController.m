//
//  PrintersViewController.m
//  EVipPOS
//
//  Created by AOC on 14/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "PrintersViewController.h"
#import "PrinterModelsViewController.h"


@interface PrintersViewController ()  {    
    
    __weak IBOutlet UILabel *lblSelectedPrinter;

}

@end

@implementation PrintersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)selectPrintAction:(id)sender {
    
    
}


- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)prepareForUnwind:(UIStoryboardSegue *) segue {
    
    PrinterModelsViewController *sourceVC = (PrinterModelsViewController *)segue.sourceViewController;
    
    int selectedPrinterIndex = sourceVC.selectedPrinterIndex;
    
    NSLog(@"%d", selectedPrinterIndex);
    
    lblSelectedPrinter.text = sourceVC.selectedPrinterName;    
    
}

@end
