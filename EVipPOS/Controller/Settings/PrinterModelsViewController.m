//
//  PrinterModelsViewController.m
//  EVipPOS
//
//  Created by AOC on 14/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "PrinterModelsViewController.h"

@interface PrinterModelsViewController () {
    
    NSArray * printers ;
}


@end

@implementation PrinterModelsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    printers = [[NSArray alloc] initWithObjects:@"No priter", @"Star TSP654IIBI (Bluetooth)", @"Star TSP143IILAN (Ethernet)", @"EPSON TM-T20II (Ethernet)", nil];
    
    self.selectedPrinterIndex = 0;
    self.selectedPrinterName = printers[0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
//    self.view.backgroundColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.3];

}
- (IBAction)okAction:(id)sender {
    
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self performSegueWithIdentifier:@"SegueForModel" sender:self];
    
}

- (IBAction)cancelAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)selectOptionAction:(id)sender {
    
    UIButton *btn = (UIButton *) sender;
    
    for (int i = 0 ; i < 4 ; i++) {
        
        UIImageView *selectedImageView = [self.view viewWithTag:i+10];
        
        if (btn.tag == selectedImageView.tag + 10) {
            
            selectedImageView.image = [UIImage imageNamed:@"ic_radio_on"];
            self.selectedPrinterIndex = i;
            self.selectedPrinterName = printers[i];
            
        } else {
            selectedImageView.image = [UIImage imageNamed:@"ic_radio_off"];
        }
    }
}



@end
